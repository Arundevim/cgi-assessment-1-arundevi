package chef.receipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class ReceipeApplication 
{
    public static void main( String[] args )
    {
        SpringApplication.run(ReceipeApplication.class, args);
    }
}
